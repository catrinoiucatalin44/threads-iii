package Laborator12;

import java.net.*;
import java.io.*;

public class UDPServer {
	public static void main(String args[]) {
       DatagramSocket bSocket = null;
       try {
           bSocket = new DatagramSocket(6789);
           
           byte[] buffer = new byte[1000];
           
           while (true) {
               DatagramPacket request = new DatagramPacket(buffer, buffer.length);
               bSocket.receive(request);
               
               DatagramPacket reply =
                       new DatagramPacket(request.getData(), request.getLength(),
                               request.getAddress(),request.getPort());
               bSocket.send(reply);
           }
       } catch
       (SocketException e){System.out.println("Socket: " + e.getMessage());
       } catch (IOException e) {System.out.println("IO: " + e.getMessage());
       } finally {if(bSocket != null) bSocket.close();}
   }
}

