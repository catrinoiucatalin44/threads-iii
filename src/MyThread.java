
public class MyThread extends Thread {
	public void run() {
		for (int i = 1; i <= 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println(e);
			}
			System.out.println(i);
		}
	}

	public static void main(String args[]) {
		MyThread t1 = new MyThread();
		MyThread t2 = new MyThread();
//		 MyThread t3 = new MyThread ();  
		t1.run();
		try {
			t1.join();
		} catch (Exception e) {
			System.out.println(e);
		}
		t2.run();
//		 t3.start();  
	}
}
