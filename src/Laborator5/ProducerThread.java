package Laborator5;

//Producer class 
class ProducerThread implements Runnable {
	Sem s;

	ProducerThread(Sem s) {
		this.s = s;
		new Thread(this, "Producer").start();
	}

	public void run() {
		for (int i = 0; i < 10; i++)
			// producer put items
			s.put(i);
	}
}
