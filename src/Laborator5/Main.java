package Laborator5;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// creating buffer queue
		Sem s = new Sem();

		// starting consumer thread
		new ConsumerThread(s);

		// starting producer thread
		new ProducerThread(s);
	}
}
