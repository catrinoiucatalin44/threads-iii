package Laborator5;

//Consumer class 
class ConsumerThread implements Runnable {
	Sem s;

	ConsumerThread(Sem s) {
		this.s = s;
		new Thread(this, "Consumer").start();
	}

	public void run() {
		for (int i = 0; i < 10; i++)
			// consumer get items
			s.get();
	}
}
