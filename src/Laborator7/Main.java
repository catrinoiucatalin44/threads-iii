package Laborator7;

public class Main {
	static final int NPTHREADS = 10;
	static final int NCTHREADS = 5;
	static final int MINN = 0;
	static final int MAXN = 76;
	static Producator pChannel[] = new Producator[NPTHREADS];
	static Consumator cChannel[] = new Consumator[NCTHREADS];
	static Channel buffer = new Channel();

	public static void main(String[] args) {
		int i;
		System.out.println("Producator - Consumator started");
		for (i = 0; i < NPTHREADS; i++) {
			pChannel[i] = new Producator(NPTHREADS, i, MINN, MAXN, buffer);
			pChannel[i].start();
		}
		for (i = 0; i < NCTHREADS; i++) {
			cChannel[i] = new Consumator(NCTHREADS, i, MINN, MAXN, buffer);
			cChannel[i].start();
		}
		for (i = 0; i < NPTHREADS; i++) {
			try {
				pChannel[i].join();
			} catch (InterruptedException e) {
			}
		}
		for (i = 0; i < NCTHREADS; i++) {
			try {
				cChannel[i].join();
			} catch (InterruptedException e) {
			}
		}
		for (i = 0; i < NPTHREADS; i++) {
			System.out.println("Producator " + i + " produced " + pChannel[i].getCounter() + " elements.");
		}
		for (i = 0; i < NCTHREADS; i++) {
			System.out.println("Consumator " + i + " consumed " + cChannel[i].getCounter() + " elements.");
		}
		System.out.println("Producator - Consumator finished");
	}
}