package Laborator6;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CountSem p = new CountSem();
		CountSem q = new CountSem();
		p.start();
		q.start();
		try {
			p.join();
			q.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Final counter: " + p.getN());
	}
}
