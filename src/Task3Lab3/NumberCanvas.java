package Task3Lab3;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;

public class NumberCanvas extends Canvas {
	int value_ = 0;
	String title_;

	Font f1 = new Font("Helvetica",Font.BOLD,36);
	Font f2 = new Font("Times",Font.ITALIC+Font.BOLD,24);

	public NumberCanvas(String title) {
	this(title,Color.cyan);
	}

	public NumberCanvas(String title,Color c) {
	super();
	title_ = title;
	setBackground(c);
	}
}
