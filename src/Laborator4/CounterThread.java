package Laborator4;

public class CounterThread extends Thread{
	
	protected Counter counter = null;
	
	public CounterThread(Counter counter) {
		this.counter = counter;
	}
	
	public void run() {
		for(int i=0; i<9 ; i++) {
			counter.add(i);
		}
	}
}
